const get = (id) => document.getElementById(id);
let h=0, m=0, s=0;
let intervalHandler = 0;

const time = () =>{
	get('timer').innerHTML = `${h}:${m}:${s}`;
	s++;
	if(s == 59){
		m++;
		s = 0;
	}
	if(m == 59){
		h++;
		m = 0;
	}
}

get('startButton').onclick = () =>{
	if(intervalHandler == 0)intervalHandler = setInterval(time, 10);
}

get('stopButton').onclick = () =>{
	clearInterval(intervalHandler);
	intervalHandler = 0;
}

get('clearButton').onclick = () =>{
	h = 0;
	m = 0;
	s = 0;
	get('timer').innerHTML = `${h}:${m}:${s}`;
}