const get = (id) => document.getElementById(id);
const screen = document.getElementById("screen");
let screenNumber = '0',
	numberA = 0,
	numberB = 0,
	operation = false,
	operationNum = 0,
	dot = false;

const claer = () =>{
	screenNumber = '0';
	numberA = 0;
	numberB = 0;
	operation = false;
	operationNum = 0;
	dot = false;
}

get('btnClear').onclick = () =>{
	claer();
	print();
}

get('btn0').onclick = () => addNum('0');
get('btn1').onclick = () => addNum('1');
get('btn2').onclick = () => addNum('2');
get('btn3').onclick = () =>	addNum('3');
get('btn4').onclick = () =>	addNum('4');
get('btn5').onclick = () =>	addNum('5');
get('btn6').onclick = () =>	addNum('6');
get('btn7').onclick = () =>	addNum('7');
get('btn8').onclick = () =>	addNum('8');
get('btn9').onclick = () =>	addNum('9');
get('btnDot').onclick = () =>{
	if (!dot){
		addNum('.');
		dot = true;
	} 
}

get('btnPM').onclick = () =>{
	screenNumber = String(+screenNumber * (-1));
	print();
	addNum('');
}

get('btnPlus').onclick = () =>{
	if(!operation){
		operation = true;
		operationNum = 1;
		dot = false;
		print();
		screenNumber = '0';
	}
}

get('btnMinus').onclick = () =>{
	if(!operation){
		operation = true;
		operationNum = 2;
		dot = false;
		print();
		screenNumber = '0';
	}
}

get('btnMulti').onclick = () =>{
	if(!operation){
		operation = true;
		operationNum = 3;
		dot = false;
		print();
		screenNumber = '0';
	}
}

get('btnDivision').onclick = () => {
	if(!operation){
		operation = true;
		operationNum = 4;
		dot = false;
		print();
		screenNumber = '0';
	}
}

get('btnProc').onclick = () => {
	if(!operation){
		operation = true;
		operationNum = 5;
		dot = false;
		print();
		screenNumber = '0';
	}
}

const addNum = (num) => {
	if(screenNumber.length < 10){
		if(screenNumber == '0'){
			screenNumber = num;
			print();
		} else{
			screenNumber += num;
			print();
		}

		if(!operation){
			numberA = +screenNumber;
			console.log(numberA);
		}else{
			numberB = +screenNumber;
			console.log(numberB);
		}	
	}
}

const print = () => {
	screen.innerHTML = screenNumber;
}

// function print(){
// 	if(screenNumber.length < 10){
// 		screen.innerHTML = screenNumber;
// 	}else{
// 		screenNumber = 'Long Result!';
// 		print();
// 		claer();
// 	}
// }

get('btnResult').onclick = () => {
	if(operation){
		switch(operationNum){
			case 1:
				screenNumber = numberA + numberB;
				print();
				claer();
			break;

			case 2:
				screenNumber = numberA - numberB;
				print();
				claer();
			break;

			case 3:
				screenNumber = numberA * numberB;
				print();
				claer();
			break;

			case 4:
				if(numberB != 0){
					screenNumber = (numberA / numberB).toFixed(8);
					print();
					claer();
				}else{
					screenNumber = 'Error';
					print();
					claer();
				}
			break;

			case 5:
				screenNumber = (numberA * numberB / 100).toFixed(8);
				print();
				claer();
			break;
		}
	}
}