var purse = {
	name: 'Vasya',
	bitcoin: {
		name: 'Bitcoin',
		logo: '<img class='imgCoin' src="./img/bitcoin.png" alt="">',
		sum: 0.4,
		exRates: 8000
	},

	ethereum: {
		name: 'Ethereum',
		logo: '<img class='imgCoin' src="./img/ethereum.png" alt="">',
		sum: 0.65,
		exRates: 5000
	},

	stellar: {
		name: 'Stellar',
		logo: '<img class='imgCoin' src="./img/stellar.png" alt="">',
		sum: 1.34,
		exRates: 3500
	},

	show: function(nameVal){
		document.write('<p>Добрый день, ' + this.name + '!</p>');
		document.write('<p>На вашем балансе' + this[nameVal].logo + '</p>');
		document.write('осталось ' + this[nameVal].sum + ' монет,</p>');
		document.write('<p>если вы сегодня продадите их то, получите ' + (this[nameVal].sum * this[nameVal].exRates) + ' грн.</p>');
		document.write('</br>');
	}
}

purse.show('bitcoin');
purse.show('ethereum');
purse.show('stellar');