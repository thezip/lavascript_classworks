let dragObject = {};
const self = this;
// const ingred = document.querySelector('.ingrediens');

document.onmousedown = (e) => {
	if (e.which != 1) { // если клик правой кнопкой мыши
    	return; // то он не запускает перенос
  	}

  	let elem = e.target.closest('.draggable');
  	if (!elem) return; // не нашли, клик вне draggable-объекта
  	dragObject.elem = elem;
  	// запомнить координаты, с которых начат перенос объекта
  	dragObject.downX = e.pageX;
  	dragObject.downY = e.pageY;
  	console.log(elem);

    //dragObject.shiftX, dragObject.shiftY - смещение
    let pos = getPosition(dragObject.elem);
    dragObject.shiftX = dragObject.downX - pos.x;
    dragObject.shiftY = dragObject.downY - pos.y;
    console.log(dragObject.shiftX, dragObject.shiftY);
    
    //создание аватара
    if (!dragObject.avatar) {
      dragObject.avatar = createAvatar(e); 
    }

    document.ondragstart = function() { return false }
    document.body.onselectstart = function() { return false }

}

document.onmousemove = (e) => {
  if (!dragObject.avatar) return; // элемент не зажат

  // let moveX = e.pageX - dragObject.downX;
  // let moveY = e.pageY - dragObject.downY;

  // // если мышь передвинулась в нажатом состоянии недостаточно далеко
  // if (Math.abs(moveX) < 5 && Math.abs(moveY) < 5) {
  //   return;
  // }

  // dragObject.avatar = createAvatar(e); //создание аватара
  // if (!dragObject.avatar) {
  //   dragObject = {};
  //   return;
  // }

 	dragObject.avatar.style.left = e.pageX - dragObject.shiftX + 'px';
  dragObject.avatar.style.top = e.pageY - dragObject.shiftY + 'px';  
}

document.onmouseup = (e) => {

  // dragObject.avatar.style.display = 'none';
  dragObject.avatar.remove();
  if (dragObject.avatar) {
    dragObject = {};
  }

  document.ondragstart = null
  document.body.onselectstart = null
  
}

//--------------------------------------------------------------
/*
function finishDrag(e) {
    
}

function startDrag(e) {
 
}

function findDroppable(event) {
    // спрячем переносимый элемент
    // dragObject.avatar.hidden = true;

    // получить самый вложенный элемент под курсором мыши
    let elem = document.elementFromPoint(event.clientX, event.clientY);

    // показать переносимый элемент обратно
    // dragObject.avatar.hidden = false;

    if (elem == null) {
      // такое возможно, если курсор мыши "вылетел" за границу окна
      return null;
    }

    return elem.closest('.droppable');
  }*/

 //-----------------------------------------------------------------

 getPosition = (elem) => {
  let box = elem.getBoundingClientRect();

  return {
    x: box.x,
    y: box.y}
  };

 createAvatar = (e) => {

  // запомнить старые свойства, чтобы вернуться к ним при отмене переноса
  //let avatar = dragObject.elem;
  let avatar = dragObject.elem.cloneNode(true);
  let old = {
      parent: avatar.parentNode,
      nextSibling: avatar.nextSibling,
      position: avatar.position || '',
      left: avatar.left || '',
      top: avatar.top || '',
      zIndex: avatar.zIndex || ''
    };

  document.body.appendChild(avatar);
  avatar.style.zIndex = 9999;
  avatar.style.position = 'absolute';

    // функция для отмены переноса
    /*avatar.rollback = function() {
      old.parent.insertBefore(avatar, old.nextSibling);
      avatar.style.position = old.position;
      avatar.style.left = old.left;
      avatar.style.top = old.top;
      avatar.style.zIndex = old.zIndex
    };*/

  return avatar;
}  